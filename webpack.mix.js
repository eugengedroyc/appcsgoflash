const fs = require('fs');
const mix = require('laravel-mix');
const combineSelectors = require('postcss-combine-duplicated-selectors');
mix
    //сборка всего vue и остального js в app.js; извлечение third-party кода в vendor.js;соединение всех third-party файлов из public/js в vendor.js
    .js('resources/js/app.js', 'public/js')
    .extract(['vue', 'vue-cookie', 'axios', 'vue-axios', 'vue-socket.io-extended', 'socket.io-client', 'vue-i18n'])
    .scripts(['public/js/vendor.js', 'public/js/noty.min.js'], 'public/js/vendor.js')
    //компиляция в css и для применения к файлам всех подключенных опций
    .less('resources/less/app.less', 'public/css')
    .less('resources/less/plugins.less', 'public/css')
    .styles('public/css/desktop.css', 'public/css/desktop.css')
    //
    .options({
        //postCss: [ ],
        extractVueStyles: false,
        processCssUrls: false,
        clearConsole: true,
    })
    .disableNotifications()

//на деве: sourcemaps для css и js;
if (!mix.inProduction()) {
    mix
        .webpackConfig({
            // plugins: [ ],
            output: {
                devtoolModuleFilenameTemplate: info => {
                    let path = 'D:/OneDrive/csgo/rustrun-master/';
                    return `file:///${path}${info.resourcePath.slice(2)}`;
                }
            },
        })
        //.browserSync('my-domain.dev')
        .sourceMaps(false, 'inline-source-map');
    //убираю файл с медиа-запросами для удобства разработки
    fs.stat('public/css/desktop.css', function (err, stats) {
        if (stats.isFile()) {
            fs.unlink('public/css/desktop.css', function (err) {
                console.error(err);
            });
        }
    })
}

//на продакшене: дополнительная оптимизация стилей; извлекаются все медиа-запросы в отдельный файл(на мобильных и старых устройствах скорость загрузки увеличиться); улучшил юзабилити без мышки 
if (mix.inProduction()) {
    mix.options({
        purifyCss: true, //purifyCss: {},
        postCss: [
            require('postcss-combine-media-query'),
            combineSelectors({
                removeDuplicatedProperties: true
            }),
            require('postcss-focus'),
            require('postcss-extract-media-query')({
                output: {
                    path: path.join(__dirname, '/public/css'),
                    name: '[query].css'
                },
                queries: {
                    'only screen and (min-width: 1023px)': 'desktop',
                    'only screen and (min-width: 1460px)': 'desktop',
                    'only screen and (min-width: 1800px)': 'desktop',
                    'only screen and (min-width: 1870px)': 'desktop',
                    'only screen and (min-width: 2100px)': 'desktop',
                    'only screen and (min-width: 2400px)': 'desktop'
                },
                extractAll: false
            })
        ]
    }).version();
}
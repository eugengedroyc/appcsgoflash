# -*- coding: UTF-8 -*-
import json
import logging
import threading
from datetime import datetime

import mysql.connector
import requests
from mysql.connector import errorcode

import config as cf

logging.basicConfig(filename='update_item.log', level=logging.DEBUG, format='%(asctime)s - %(message)s')


def update_items():
    try:
        response = requests.get(cf.price_request)
        if response.status_code == 200:
            response_data = json.loads(response.content.decode())
            if response_data.get('success', 'false'):
                cnx = init_db_connection()
                if cnx:
                    cursor = cnx.cursor()
                    items = response_data.get('items', {})
                    qual_values = []
                    non_qual_values = []
                    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    logging.debug('-----------------------')
                    logging.debug('Starting update procedure')
                    counter = 0
                    for item in items:
                        counter += 1
                        name = item['market_hash_name']
                        price = item['price']
                        has_qual = False
                        for q in cf.qual_levels:
                            if q in name:
                                has_qual = True
                                name = name.replace(' ' + q, '')
                                qual = q.replace('(', '').replace(')', '')
                                qual_values.append((price, now, name, qual))
                                continue
                        if not has_qual:
                            non_qual_values.append((price, now, name))
                        if len(non_qual_values) >= cf.items_per_req or len(items) == counter and len(
                                non_qual_values) > 0:
                            cursor.executemany(cf.non_qual_query, non_qual_values)
                            cnx.commit()
                            logging.debug(str(cursor.rowcount) + " item(s) updated")
                            non_qual_values = []
                        if len(qual_values) >= cf.items_per_req or len(items) == counter and len(qual_values) > 0:
                            cursor.executemany(cf.qual_query, qual_values)
                            cnx.commit()
                            logging.debug(str(cursor.rowcount) + " item(s) updated")
                            qual_values = []
                    cnx.close()
                else:
                    logging.debug('Connection failed. Reason unknown.')
        else:
            logging.debug('Wrong status code - ' + response.status_code)
            threading.Timer(60, update_items).start()
        logging.debug('End of update iteration')
        logging.debug('-----------------------')
        threading.Timer(60 * cf.update_timeout, update_items).start()
    except Exception as e:
        logging.debug(e)
        logging.debug('Restarted after exception')
        threading.Timer(60, update_items).start()


def init_db_connection():
    try:
        cnx = mysql.connector.connect(user=cf.dbdata['user'],
                                      password=cf.dbdata['password'],
                                      host=cf.dbdata['host'],
                                      database=cf.dbdata['db'],
                                      port=cf.dbdata['port'])
        return cnx
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    return False


update_items()

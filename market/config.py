# -*- coding: UTF-8 -*-
# параметры подключения к бд
dbdata = {
    'user': 'root',
    'password': 'fhf4@dsi^4ac~(a%)a5k1jB8Xa6xV6FLP0m',
    'host': 'localhost',
    'db': 'rustrun',
    'port': 3306
}

items_per_req = 500  # количество строк для записи за одну транзакцию

price_request = 'https://market.csgo.com/api/v2/prices/USD.json'  # запрос к маркету на получение списка цен

qual_query = "UPDATE all_items SET price = %s, updated_at = %s " \
             "WHERE market_hash_name = %s and exterior = %s"  # запрос в бд для обновления
non_qual_query = "UPDATE all_items SET price = %s, updated_at = %s " \
                 "WHERE market_hash_name = %s"  # запрос в бд для обновления

update_timeout = 60  # таймаут для обновления цен в минутах

qual_levels = ['(Factory New)', '(Minimal Wear)', '(Field-Tested)', '(Well-Worn)', '(Battle-Scarred)']  # степени износа предметов
<?php

namespace App\Http\Controllers\Admin;

use App\AllItem;
use App\Http\Controllers\AllItemsController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

ini_set('max_execution_time', '0');

class ItemsController extends Controller
{
    public function index()
    {
        return view('admin.items.index');
    }

    public function create()
    {
        return view('admin.items.create');
    }

    public function createPost(Request $request)
    {
        AllItem::query()->create($request->all());

        return redirect('/admin/items');
    }

    public function edit($id)
    {
        $item = AllItem::query()->find($id);

        if (!$item) {
            return redirect()->back();
        }

        return view('admin.items.edit', compact('item'));
    }

    public function editPost($id, Request $r)
    {
        AllItem::query()->find($id)->update($r->all());

        return redirect('/admin/items/edit/' . $id);
    }

    public function delete($id)
    {
        AllItem::query()->find($id)->delete();

        return redirect()->back();
    }

    public function prices()
    {
        $prices = json_decode(file_get_contents('https://market.csgo.com/api/v2/prices/USD.json'), true);

        if (!$prices['success']) {
            return 'Ошибка загрузки цен';
        }

        $newPrices = [];

        foreach ($prices['items'] as $price) {
            $newPrices[$price['market_hash_name']] = $price['price'];
        }

        foreach (AllItem::query()->get() as $itemDB) {
            $fullName = $itemDB->market_hash_name . ' (' . $itemDB->exterior . ')';
            $fullName = str_replace('StatTrak™', '', $fullName);

            if (!isset($newPrices[$fullName])) {
                $itemDB->delete();
                continue;
            }

            $itemDB->update(['price' => round($newPrices[$fullName], 2)]);
        }

        return redirect()->back();
    }
}

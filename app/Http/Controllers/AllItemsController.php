<?php

namespace App\Http\Controllers;

use App\AllItem;
use Illuminate\Http\Request;

ini_set('max_execution_time', '0');

class AllItemsController extends Controller
{
    public function fill()
    {
        $prices = json_decode(file_get_contents('https://market.csgo.com/api/v2/prices/USD.json'), true);

        if (!$prices['success']) {
            return 'Ошибка загрузки цен';
        }

        $newPrices = [];

        foreach ($prices['items'] as $price) {
            $newPrices[$price['market_hash_name']] = $price['price'];
        }

        foreach (AllItem::query()->get() as $itemDB) {
            $fullName = $itemDB->market_hash_name . ' (' . $itemDB->exterior . ')';
            $fullName = str_replace('StatTrak™', '', $fullName);

            if (!isset($newPrices[$fullName])) {
                $itemDB->delete();
                continue;
            }

            $itemDB->update(['price' => round($newPrices[$fullName], 2)]);
        }

        return 'Цены обновлены';
    }

    public function getList(Request $r)
    {
        try {
            $minPrice = $r->get('minPrice');
            $maxPrice = $r->get('maxPrice');
            $marketHashName = $r->get('market_hash_name');
            $page = $r->get('page') ? $r->get('page') : 1;

            return \Cache::remember('items_type_'. $minPrice .'_p_'. $maxPrice.'_name_'. $marketHashName .'_page_' . $page, 3600, function () use ($minPrice, $maxPrice, $marketHashName, $page) {
                return AllItem::query()
                    ->where('price', '>=', $minPrice)
                    ->where('price', '<=', $maxPrice)
                    ->where('market_hash_name', 'like', '%' . $marketHashName . '%')
                    ->orderBy('price', 'DESC')
                    ->paginate(18, ['*'], 'page', $page);
            });
        } catch (\Exception $e) {
            return [];
        }
    }
}
